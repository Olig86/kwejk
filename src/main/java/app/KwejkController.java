package app;

import app.repo.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class KwejkController {

    @Autowired
    private ImageRepository imageRepo;

    @GetMapping("/")
    public String hello(ModelMap modelMap) {
        modelMap.addAttribute("attr", "My favorites");
        modelMap.addAttribute("fileNameList", imageRepo.listAllGifs());
        return "Hello";
    }

}
