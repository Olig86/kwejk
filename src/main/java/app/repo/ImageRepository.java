package app.repo;

import java.util.List;

public interface ImageRepository {

    List<String> listAllGifs();

}
