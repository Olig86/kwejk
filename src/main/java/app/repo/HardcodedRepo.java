package app.repo;

import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component(value = "hardRepo")
public class HardcodedRepo implements ImageRepository {

    private List<String> hardRepo;

    public HardcodedRepo() {
        hardRepo = new LinkedList<>();
        hardRepo.add("vincent.gif");
        hardRepo.add("andrzej.gif");
    }

    @Override
    public List<String> listAllGifs() {
        return hardRepo;
    }
}
