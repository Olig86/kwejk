package app.repo;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Component(value = "imageRepo")
public class RepoFromFiles implements  ImageRepository{

    private List<String> gifsList;

    public RepoFromFiles() {
        URL url = getClass().getResource("/static/images");
        Path pathToFiles = null;
        try {
            pathToFiles = Paths.get(url.toURI());
            gifsList = Files.list(pathToFiles)
                    .map(Path::getFileName)
                    .map(fileName -> "images/" + fileName)
                    .collect(toList());

            gifsList.stream()
                    .forEach(System.out::println);

        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<String> listAllGifs() {
        return gifsList;
    }
}
